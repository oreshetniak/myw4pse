import $ from 'jquery';
import { Swiper, SwiperSlide, Pagination, Navigation, Scrollbar,Autoplay }  from 'swiper';

Swiper.use([Pagination,Navigation, Scrollbar,Autoplay]);

window.$ = $;