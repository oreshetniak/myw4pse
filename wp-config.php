<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'myw4pse' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ysxTdg<,k@~UbVyT.g94U,a 6zTiX0![yZ|$~},y);&8fMS+1QbRO2BV|571c6(m' );
define( 'SECURE_AUTH_KEY',  '2lexv.K~q-&&y5T!fi~uye@ck1JO*nURu<43mENYogbKcp~@v$;Y8fD_6QC6YBNC' );
define( 'LOGGED_IN_KEY',    'Ebfi55%b/,>7R}gp6aCK4{}mmYB7B$bE{hXn4mILrLM(zxzsWg{y<{9J}3$bv8<6' );
define( 'NONCE_KEY',        'zDkivEM:oTaY[_^XyC.9&8v*js[}SWyTQ@UY_b-Mnn`T^~.g2;>)yw.;mdaQ@S$W' );
define( 'AUTH_SALT',        'g!^I9V&N5,PKwX4A(bS>S8Sf S|pl.LV3B6vkh#;M9YYscln+v`M=Az qEx.ZTB~' );
define( 'SECURE_AUTH_SALT', 'Hh;;bZd2;-+bg)lCV2F`;B9Qo3*&[QPGf;.!Ns/2=5H[G.RzD~1EJc<Bz@2H4a,4' );
define( 'LOGGED_IN_SALT',   '-oPOhqbFxUtWfq1{p=jOxzVaM&`9E4AyW>F+#^;!5-x:7RE5^hq<WD)h2=0M;>O=' );
define( 'NONCE_SALT',       '3F>?wGvt!{EgQmTu(sJqH//7n~|O7asbuq0CARdYQD.dIDmU[)vcB#,]#R`~3X2n' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
